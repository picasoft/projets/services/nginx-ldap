FROM debian:bullseye
ARG NGINX_VERSION=1.23.2
LABEL maintainer picasoft@assos.utc.fr

# Install prerequisites
RUN apt-get update && \
    apt-get install -y build-essential git-core libpcre3-dev libldap2-dev libssl-dev wget zlib1g zlib1g-dev

# Download LDAP module
RUN git clone https://github.com/kvspb/nginx-auth-ldap.git /nginx-auth-ldap

# Download and build NGINX with LDAP module
RUN wget -O nginx.tar.gz http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
    tar xvf nginx.tar.gz && \
    cd nginx-${NGINX_VERSION} && \
    chmod +x configure && \
    ./configure --user=nginx --group=nginx --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-http_gzip_static_module --with-http_stub_status_module --with-http_ssl_module --with-pcre --with-file-aio --with-http_realip_module --add-module=/nginx-auth-ldap/ --with-ipv6 --with-debug && \
    make && \
    make install

COPY ./entrypoint.sh /entrypoint.sh
COPY ./nginx.conf /etc/nginx/templates/nginx.conf.template

RUN touch /etc/nginx/site.conf

RUN apt-get install -y gettext-base && \
    chmod +x /entrypoint.sh

RUN useradd nginx

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx"]
