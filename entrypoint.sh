#!/bin/sh

# Hackish way to comment line if methods are empty
if [ -z ${METHODS_WITHOUT_AUTH} ]; then
  COMMENT='#'
fi

# Default port
LISTEN_PORT="${LISTEN_PORT:-80}"

# Substitute env variables (secrets) in nginx configuration
LISTEN_PORT=${LISTEN_PORT} COMMENT=${COMMENT} envsubst < /etc/nginx/templates/nginx.conf.template > /etc/nginx/nginx.conf

# Execute initial command
exec $@
