## nginx with LDAP authentication

nginx has a mechanism allowing to use an external service handling authentication requests. For LDAP, the traditional way involves running a [ldap-auth](https://github.com/nginxinc/nginx-ldap-auth) daemon. It seemed a bit burdensome to me, even if there is a [Docker image](https://hub.docker.com/r/bitnami/nginx-ldap-auth-daemon/) for the daemon. I think this is because LDAP auth is included in NGINX Plus and a bit tricky to integrate with free pre-built packages.

There is an unofficial nginx module, [nginx-auth-ldap](https://github.com/kvspb/nginx-auth-ldap), which works well. We just need to recompile nginx with it, and voilà.

This is the purpose of this repository : make a ready-to-use nginx single image with LDAP auth.
The nominal use is pretty standard : act as an authorization proxy or serve a simple single website.

## Sample usage with Compose

```yaml
version: '3.8'

services:
  nginx_ldap:
    image: registry.picasoft.net/pica-nginx-ldap:1.21.4
    container_name: registry_frontend
    environment:
      LDAP_URL: ldaps://ldap.picasoft.net:636
      LDAP_BASE_DN: dc=picasoft,dc=net
      LDAP_ANSWER_ATTRIBUTES: cn
      LDAP_SCOPE_SEARCH: sub
      LDAP_FILTER: (objectClass=posixAccount)
      LDAP_BIND_DN: cn=nss,dc=picasoft,dc=net
      METHODS_WITHOUT_AUTH: GET
      SERVER_NAME: registry.picasoft.net
      LISTEN_PORT: 80
    ports:
      - 8080:80
    env_file: ./secrets/ldap.secrets
    restart: unless-stopped
```

With `LDAP_BIND_PASSWORD=XXX` in `ldap.secrets`.

**This example should be used for testing purpose or behind an SSL reverse-proxy, otherwise all LDAP credentials will be sent as clear text.**
To add SSL capabilities to nginx, see below.

## Build

```
docker build -t nginx-ldap .
```

## LDAP configuration

Via Environments variables :

- `LDAP_URL` → `ldap[s]://<url>:<port>`
- `LDAP_BASE_DN` → where to start the search
- `LDAP_ANSWER_ATTRIBUTES` → which attributes to get back, comma-separated.
- `LDAP_SCOPE_SEARCH` : `base`, `one` or `sub`
- `LDAP_FILTER` : constrain the search
- `LDAP_BIND_DN` : user with read access
- `LDAP_BIND_PASSWORD` : password for the user with read access
See [official docs](https://ldapwiki.com/wiki/LDAP%20URL) for detail.

## Server configuration

### Environment variables

- `SERVER_NAME` : URL of your website.
- `LISTEN_PORT` : listening port, usually `80` or `443 ssl`. See [nginx documentation](http://nginx.org/en/docs/http/ngx_http_core_module.html#listen). *default: 80*
- `METHODS_WITHOUT_AUTH` : space-separated HTTP methods which bypass LDAP auth.

### Additional configuration

Mount a file at `/etc/nginx/site.conf`. Every line will be included in the `server { location / {}}` section of `nginx.conf`.
If you use SSL you will need to mount a file with at least these directives :

```
# SSL
ssl_certificate /etc/nginx/conf.d/domain.crt;
ssl_certificate_key /etc/nginx/conf.d/domain.key;

# Recommendations from https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
ssl_protocols TLSv1.1 TLSv1.2;
ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
ssl_prefer_server_ciphers on;
ssl_session_cache shared:SSL:10m;
```

Environment variables **won't be** substituted at startup.

### Special case : nested locations

If you add another `location` block in the mounted `site.conf` and you want some methods to bypass LDAP auths, add this block :

```
limit_except METHOD1 METHOD2 ... {
  auth_ldap "Enter LDAP credentials";
  auth_ldap_servers main;
}
```

This is because access limitations are not inherited from parent block.
